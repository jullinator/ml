from sklearn.datasets import fetch_mldata
from sklearn.preprocessing import StandardScaler
from sklearn.linear_model import LinearRegression
from sklearn.model_selection import cross_val_score, RandomizedSearchCV, GridSearchCV
from sklearn.ensemble import RandomForestClassifier

mnist = fetch_mldata("MNIST original")
x,y = mnist.data, mnist.target
test_index = 60_000
x_train, y_train = x[:test_index], y[:test_index]
x_test, y_test = x[test_index:], y[test_index:]

scaler = StandardScaler()
x_train = scaler.fit_transform(x_train)

model = RandomForestClassifier()
param_grid = [
    dict(n_estimators=[3, 10, 30], max_features=[500, 750, 784]),
]
grid_search = GridSearchCV(model, param_grid)
grid_search.fit(x_train, y_train)
model = grid_search.best_estimator_

scores = cross_val_score(model, x_train, y_train,  scoring="accuracy")
print(scores)