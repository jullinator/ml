import os
import pandas as pd
import numpy as np
HOUSING_PATH = os.path.join("datasets", "housing")

def load_housing_data():
    csv_file = os.path.join(HOUSING_PATH, "housing.csv")
    return pd.read_csv(csv_file)


def split_train_test(data = load_housing_data(), test_ratio = 0.2):
    shuffled_indices = np.random.permutation(len(data))
    test_len = int(len(data) * test_ratio)
    test_indices = shuffled_indices[:test_len]
    train_indices = shuffled_indices[test_len:]
    return data.iloc[train_indices], data.iloc[test_indices]

if __name__ == '__main__':
    split_train_test()