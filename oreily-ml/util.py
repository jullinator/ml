import tarfile
import os 
from six.moves import urllib

DOWNLOAD_ROOT = "https://raw.githubusercontent.com/ageron/handson-ml/master/"
DESTINATION = "datasets"
HOUSING_URL = DOWNLOAD_ROOT + "datasets/housing/housing.tgz"

def download(url, name):
    destination = os.path.join(DESTINATION, name)

    if not os.path.isdir(DESTINATION):
        os.mkdir(DESTINATION)
    if not os.path.isdir(destination):
        os.mkdir(destination)

    tgz_path = os.path.join(destination, "{}.tgz".format(name))
    urllib.request.urlretrieve(url, tgz_path)
    tgz_file = tarfile.open(tgz_path)
    tgz_file.extractall(path = destination)
    tgz_file.close()

if __name__ == '__main__':
    download(HOUSING_URL, "housing")

