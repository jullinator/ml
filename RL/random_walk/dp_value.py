
from random import shuffle

A,B,C,D,E = 1,2,3,4,5
states = [A,B,C,D,E]
org_states = states.copy()

value_functions = [
    0,
    0,0,0,0,0,
    1
]

for i in range(100):
    shuffle(states)
    for state in states:
        value_functions[state] = 0.5*value_functions[state-1] + 0.5*value_functions[state+1]
    if i%1 == 0:
        for state in org_states:
            print(value_functions[state], end=" ")
        print()
        