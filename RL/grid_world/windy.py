# Uses Sarsa (e-greedy), e=1/t 
from enum import Enum 
from collections import defaultdict
""" From the book; grid world, but with wind""" 

MIN_Y = 0
MIN_X = 0
MAX_X = 5   # 6 columns
MAX_Y = 5   # 6 rows


class Actions(Enum):
    LEFT = 0
    UP = 1
    RIGHT = 2
    DOWN = 3 

class Agent:
    """ SARSA """
    Q = defaultdict(lambda: 0)
    epsilon = 0.1

class Enviroment:
    winds = [0,0,1,1,2,2,1,1]

class State:
    def __init__(self, x, y):
        self.x = x 
        self.y = y

