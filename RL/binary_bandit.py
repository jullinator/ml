from random import random, randint

class Bandit:    
    def __init__ (self, prob = 0.5):
        self.prob = prob 
    
    def spin(self):
        return random() < self.prob


A = [Bandit(0.2), Bandit(0.1)]
B = [Bandit(0.9), Bandit(0.8)]
B2 = [Bandit(0.9), Bandit(0.3)]
C = [Bandit(0.55), Bandit(0.45)]

bandits = []
Q = []
K = []
I = 10000

def choose_action(epsilon):
    if random() < epsilon:
        return randint(0, len(bandits)-1)
    else:
        return  Q.index(max(Q))

def bandit(a):
    K[a] += 1
    return bandits[a].spin()

def update(a, reward):
    Q[a] = Q[a] + (1/K[a])*(reward-Q[a])

def test(epsilon, _bandits):
    global bandits, Q, K
    bandits = _bandits
    Q = [0.5 for i in range(len(bandits))]
    K = [0 for i in range(len(bandits))]
    res = 0
    for i in range(I):
        a = choose_action(epsilon)
        reward = bandit(a)
        update(a,reward)
        res += reward
    return res/I

def test_x(arr):
    print("eps=1 ", test(1, arr))
    print("eps=0.1 ", test(0.1, arr))
    print("eps=0.01 ", test(0.01, arr))

test_x(B2)