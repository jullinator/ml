from collections import defaultdict
from enum import Enum, unique
from random import randint

@unique
class Action(Enum):
    STICK = 0
    HIT = 1

class Card:
    def __init__(self, value):
        self.value = value

#TODO: Avoid global variables
cards = [
    Card(1), Card(2), Card(3), Card(4), Card(5), Card(6),
    Card(7), Card(8), Card(9), Card(10), Card(10), Card(10), Card(10) 
]

class State:
    def __init__(self, usable_ace = False, player_value = 4, dealer_showing = 2):
        self.usable_ace = usable_ace
        self.player_value = player_value
        self.dealer_showing = dealer_showing

    def hashed(self):
        return (self.usable_ace, self.player_value, self.dealer_showing)


def get_cards_value(cards = []):
    val = sum(cards)
    num_aces = cards.count(1)
    aces_left = num_aces
    while aces_left > 0 and val + 10 <= 21:
        val += 10 
    return val 

def generate_card():
    return cards[randint(0, len(cards) -1)]

class Enviroment:
    states = []
    player_full = False 

    def __init__ (self):
        self.player_cards = [generate_card().value, generate_card().value]
        self.dealer_cards = [generate_card().value]
        init_val = sum(self.player_cards)
        val = get_cards_value(self.player_cards)
        usable_ace = val > init_val
        self.state = State(usable_ace, val, self.dealer_cards[0])   

    def do_action(self, action):
        # Implied that action is Action.HIT
        card = cards[randint(0, len(cards)-1)]
        self.player_cards.append(card.value)

        init_val = sum(self.player_cards)
        val = get_cards_value(self.player_cards)
        usable_ace = val > init_val   

        if val > 21:
            self.player_full = True 
        else:
            self.state = State(usable_ace, val, self.dealer_cards[0])    
            self.states.append(self.state)


    def determine_reward(self):
        """ policy of dealer: """
        val = 0
        while True:
            val = get_cards_value(self.dealer_cards)
            if val <= 17:
                self.dealer_cards.append(generate_card().value)
            elif val > 21:
                return 1 
            else:
                break
        player_val = get_cards_value(self.player_cards)
        if val < player_val:
            return 1
        elif val > player_val:
            return -1
        else:
            return 0 
        


class Agent:

    state_values = defaultdict(lambda : 0.5)
    state_visits = defaultdict(int)
    
    def next_action(self, state= State()):
        """ Uses policy from book hit if not 20 or 21 """
        if state.player_value >= 17:
            return Action.STICK
        return Action.HIT


    def update_values (self, reward, states):
        for state in states:
            key = state.hashed()
            self.state_visits[key] += 1
            visits, prev_val = self.state_visits[key], self.state_values[key]
            self.state_values[key] = prev_val + (1/visits) * (reward - prev_val) 
    



def new_episode(env=Enviroment(), agent=Agent()):
    reward = 0 

    while reward == 0:
        action = agent.next_action(env.state)
        if action == Action.STICK:
            break 
        env.do_action(action)   # messy since it will always send Action.HIT
        if env.player_full:
            reward = -1


    if reward == 0:
        reward = env.determine_reward() # Plays out and compares cards of dealer vs player; reward in range(-1,1) ink. 0
    return reward, env.states

class Simulation:
    def __init__ (self, iterations = int(1e5)):
        self.iterations = iterations


    def generate(self):
        agent = Agent()
        for i in range(self.iterations):
            env = Enviroment()
            reward, states = new_episode(env, agent)
            agent.update_values(reward, states)
        
        print(dict(agent.state_values))
        print(len(agent.state_visits.keys()))
        print(sum(agent.state_visits.values()))


Simulation(1000).generate()

